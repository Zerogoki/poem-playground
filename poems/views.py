import random

# from django.http import HttpResponse
from django.shortcuts import render, redirect

# Create your views here.
from poems.models import *


def stories(request):
    completed = Poem.objects.filter(ended=True)
    current = Poem.objects.filter(ended=False)
    return render(request, "stories.html", {'stories_comp': completed, 'stories_cur': current})


def write(request):
    if request.method == 'GET':
        if 's' not in request.GET:
            return redirect('/')
        idd = request.GET['s']
        poem = Poem.objects.get(pk=idd)
        title = poem.title
        paragraphs = Paragraph.objects.filter(poem=poem)
        text = list(paragraphs)[-1].text
        return render(request, 'writer.html', {'title': title, 'text': text, 'idd': poem.id,
                                               'ended': poem.ended})

    if request.method == 'POST':
        idd = request.POST['id']
        nickname = request.POST['name']
        text = request.POST['new']
        author = find_author(request, nickname)
        if len(text) > 1000:
            return redirect('/')
        # находим нашу поэму с конкретным id...
        poem = Poem.objects.get(pk=idd)
        paragraph = Paragraph()
        paragraph.author = author
        if text != '':
            paragraph.text = text
        paragraph.poem = poem
        if request.POST['button'] == 'Закончить':
            poem.ended = True
        poem.save()
        paragraph.save()

        return redirect('/write?s=%s' % idd)


def new(request):
    if request.method == 'GET':
        return render(request, "add.html")
    if request.method == 'POST':
        nickname = request.POST['name']
        text = request.POST['new']
        title = request.POST['title']
        if len(title) > 100 or len(text) > 1000:
            return redirect('/')

        poem = Poem()
        paragraph = Paragraph()
        author = find_author(request, nickname)
        poem.title = title
        poem.author = author
        poem.save()

        paragraph.author = author
        paragraph.poem = poem
        paragraph.text = text

        paragraph.save()

        return redirect('/')


def view(request):
    poem_id = request.GET['s']
    poem = Poem.objects.get(pk=poem_id)
    paragraphs = Paragraph.objects.filter(poem=poem)
    return render(request, 'view.html', {'s': poem, 'pars': paragraphs})


def find_author(request, nickname):
    if nickname == "" or len(nickname) > 30:
        nickname = "Anonymous"
    authors = Author.objects.filter(nickname=nickname)
    if len(authors) > 0:
        author = authors[0]
    else:
        author = Author()
        author.nickname = nickname
        author.ip_addr = request.META.get('REMOTE_ADDR')
        author.save()
    return author
