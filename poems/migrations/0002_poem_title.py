# Generated by Django 2.2.3 on 2019-07-18 08:08

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('poems', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='poem',
            name='title',
            field=models.TextField(default=''),
            preserve_default=False,
        ),
    ]
