from django.db import models

# Create your models here.


class Author(models.Model):
    nickname = models.TextField()
    ip_addr = models.TextField()


class Poem(models.Model):
    title = models.TextField()
    ended = models.BooleanField(default=False)
    author = models.ForeignKey(Author, on_delete=models.CASCADE)


class Paragraph(models.Model):
    poem = models.ForeignKey(Poem, on_delete=models.CASCADE)
    text = models.TextField()
    author = models.ForeignKey(Author, on_delete=models.CASCADE)
