from django.contrib import admin

# Register your models here.

from .models import Poem, Paragraph, Author

admin.site.register(Poem)
admin.site.register(Paragraph)
admin.site.register(Author)
